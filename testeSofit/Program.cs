﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace testeSofit
{
    //Classe para armazenar os dados dos JSONs
    public class Post
    {
        public string date { get; set; }
        public double value { get; set; }

        public Post(string date, double value)
        {
            this.date = date;
            this.value = value;
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            //Repostas dos GETs de cada URL
            object precos = getDadosFromUrl("https://challenge-for-adventurers.herokuapp.com/data/5e34c8b0f7cb4c001480050a/prices");
            object abastecimentos = getDadosFromUrl("https://challenge-for-adventurers.herokuapp.com/data/5e34c8b0f7cb4c001480050a/supplies");
            object quilometragens = getDadosFromUrl("https://challenge-for-adventurers.herokuapp.com/data/5e34c8b0f7cb4c001480050a/spents");
            
            //Conversão de JSON para classe Post
            var postPrecos = JsonConvert.DeserializeObject<List<Post>>(precos.ToString());
            var postAbastecimentos = JsonConvert.DeserializeObject<List<Post>>(abastecimentos.ToString());
            var postQuilometragens = JsonConvert.DeserializeObject<List<Post>>(quilometragens.ToString());

            int indexPrecos = 0;          //contador de índice para os valores de preço
            int indexAbastecimentos = 0;  //contador de índice para os valores de abastecimento
            int indexQuilometragens = 0;  //contador de índice para os valores de quilometragem
            double precoDia = 0;          //armazena o preço do combustível dependendo da data analisada
            double tanque = 0;            //saldo de combustível até a data analisada
            int consumo = 9;              //consumo do veículo em km/l

            List<Post> resultado = new List<Post>();  //Lista onde será armazenada os dados de saldo de combustível {date, value}
            DateTime dataInicial = DateTime.Parse("02/01/2017"); //data inicial para iteração (baseada nos dados fornecidos)

            while(dataInicial <= DateTime.Parse("13/07/2018"))   //iteração até a data máxima (baseada nos dados fornecidos)
            {
                //verifica se há alteração de preço de combustível no dia. Em caso positivo atualiza o preço e incrementa o índice dos preços
                if (postPrecos.Count > indexPrecos && postPrecos[indexPrecos].date.Equals(dataInicial.ToShortDateString()))  
                {
                    precoDia = postPrecos[indexPrecos].value;
                    indexPrecos++;
                }

                //verifica se houve abastecimento no dia. Em caso positivo acrescenta o valor no tanque e incrementa o índice de abastecimentos
                if (postAbastecimentos.Count > indexAbastecimentos && postAbastecimentos[indexAbastecimentos].date.Equals(dataInicial.ToShortDateString()))
                {
                    tanque += Math.Round(postAbastecimentos[indexAbastecimentos].value / precoDia, 2);
                    tanque = Math.Round(tanque, 2);
                    indexAbastecimentos++;
                }

                //verifica se houve gasto no dia. Em caso positivo desconta o valor no tanque e incrementa o índice de quilometragens
                if (postQuilometragens.Count > indexQuilometragens && postQuilometragens[indexQuilometragens].date.Equals(dataInicial.ToShortDateString()))
                {
                    tanque -= Math.Round(postQuilometragens[indexQuilometragens].value / consumo, 2);
                    tanque = Math.Round(tanque, 2);
                    indexQuilometragens++;
                }

                //adiciona o saldo do tanque no dia na lista
                resultado.Add(new Post(dataInicial.ToShortDateString(), tanque));

                //incrementa o valor da data para a próxima iteração
                dataInicial = dataInicial.AddDays(1);
            }
            
            //conversão da lista para JSON
            var json = JsonConvert.SerializeObject(resultado);

            //POST com o payload do resultado
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://challenge-for-adventurers.herokuapp.com/check?id=5e34c8b0f7cb4c001480050a");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            //resposta do POST
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }
        }

        //função para fazer um GET passando uma URL como parâmetro
        static object getDadosFromUrl(string url)
        {
            var requisicaoWeb = WebRequest.CreateHttp(url);
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "Dados";

            object objResponse = null;
            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                objResponse = reader.ReadToEnd();
                streamDados.Close();
                resposta.Close();
            }
            return objResponse;
        }
    }
}
